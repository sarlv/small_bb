/**
 * @author Levon Sarkisov
 */
(function(global) {

    'use strict';

    if (!Array.prototype.forEach) {
        Array.prototype.forEach = function(fn, scope) {
            var i = 0,
                len = this.length;
            for (; i < len; ++i) {
                fn.call(scope || this, this[i], i, this);
            }
        }
    }

    if (!Function.prototype.bind) {
        Function.prototype.bind = function(context) {
            var self = this,
                args = Array.prototype.slice.call(arguments, 1);

            return function() {
                return self.apply(context, new Array(args, arguments));
            }
        }
    }

    function isArray(obj) {
        if (Object.prototype.toString.call(obj) !== '[object Array]') {
            return false;
        }
        return true;
    }

    function addListener(elem, event, fn) {
        if (elem.addEventListener) {
            elem.addEventListener(event, fn, false);

        } else if (elem.attachEvent) {
            elem.attachEvent('on' + event, (function(el) {
                return function() {
                    fn.call(el, window.event);
                };
            }(elem)));

            elem = null;
        }
    }

    function ajx(param) {
        var http;

        if (!http && typeof XMLHttpRequest != 'undefined') {
            http = new XMLHttpRequest();
        } else {
            try {
                http = new ActiveXObject('Msxml2.XMLHTTP');
            } catch (e) {
                try {
                    http = new ActiveXObject('Microsoft.XMLHTTP');
                } catch (E) {
                    http = false;
                }
            }
        }

        http.onreadystatechange = function() {

            if (http.readyState == 4) {

                //param.element.innerHTML = http.statusText;

                if (http.status == 200) {
                    param.success(http.responseText);
                }
            }

        }

        http.onerror = function(err) {
            param.error(err);
        }

        http.open(param.method, param.url, true);
        http.send(null);

    }

    function _extend(target, obj) {
        var i;
        for (i in obj) {
            if (obj.hasOwnProperty(i)) {
                target[i] = obj[i];
            }
        }
        return target;
    };

    var Mockup = Mockup || Object.create(null);

    Mockup.on = addListener;

    var Events = Mockup.Events = {
        _registry: Object.create(null),

        pub: function(event, params) {
            if (!this._registry[event]) {
                return;
            }
            var params = [].slice.call(arguments, 1);
            this._registry[event].forEach(function(handler) {
                handler.apply(this, params);
            });

        },

        sub: function(event, handler) {
            if (!this._registry[event]) {
                this._registry[event] = [];
            }
            this._registry[event].push(handler);
        },

        off: function(event) {
            if (!this._registry[event]) {
                return;
            }
            var i;
            for (i in this._registry) {
                delete this._registry[i];
            }
        }

    };

    var Model = Mockup.Model = function(methods) {
        this.data = [];
        _extend(this, methods);
    };

    Mockup.Model.prototype = {
        _uid: 0,

        uid: function(prefix) {
            var id = ++this._uid + '';
            return prefix ? prefix + id : id;
        },

        get: function(first, last) {
            if (!last) {
                return data[first];
            }
            return this.data.slice(first, last);
        },

        set: function(obj) {
            if (isArray(obj)) {
                var self = this;
                obj.forEach(function(item) {
                    if (!item.id) {
                        item.id = self.uid();
                    }
                    self.data.push(item)
                });

                return false;
            }

            obj.id = this.uid();
            this.data.push(obj);
            this.pub('data-changed', null);
        },

        remove: function(id) {
            var i = 0,
                data = this.data,
                len = data.length;

            data.forEach(function(item) {
                if (item.id === id) {
                    data.splice(i, 1);
                }
                i++;
            });

            if (len > this.data.length) {
                this.pub('data-changed', null);
            }
        },

        ajx: ajx
    }

    _extend(Mockup.Model.prototype, Mockup.Events);

    var View = Mockup.View = function(methods) {
        _extend(this, methods);
    };

    Mockup.View.prototype = {
        template: function(el, params) {
            var i,
                htm = '';

            params.forEach(function(obj, num) {
                htm += el.innerHTML.replace(/\{{\s+(\w*)\s+\}}/g, function(e, i) {
                    return obj[i];
                });
            });

            return htm;
        }
    };

    _extend(Mockup.View.prototype, Mockup.Events);

    var Controller = Mockup.Controller = function(methods) {
        _extend(this, methods);

        var self = this;

        if (self.events) {

            var i,
                l,
                el,
                fn,
                evt,
                parts;

            for (i in self.events) {
                parts = i.split(' ');
                el = self.elem(parts[0]);
                evt = parts[1];
                fn = self[self.events[i]];

                if (!el) {
                    return false;
                }

                l = el.length;

                while (l--) {
                    (function(f) {
                        addListener(el[l], evt, function(e) {
                            f.call(this, e, self);
                        });
                    }(fn));
                }

            }
        }

        if (!self.init) {
            throw new Error('You have to define method "init"');
        }

        self.init();

        if (!self.render) {
            throw new Error('You have to define method "render"');
        }

        self.sub('data-changed', self.render);
    };

    Mockup.Controller.prototype = {
        id: function(el) {
            var el = el || this.el;

            return document.getElementById(el);
        },

        elem: function(el) {
            var el = el || this.el;

            return document.querySelectorAll(el);
        }
    }

    _extend(Mockup.Controller.prototype, Mockup.Events);


    global.Mockup = Mockup;

}(typeof window !== 'undefined' ? window : this));
