describe("Base.js library", function() {
    it("contains spec with an expectation", function() {
        
        var model = new Mockup.Model({
        	data: []
        });

        var num = 0;

        var ctrl = new Mockup.Controller({
        	init: function() {
        		this.render();
        	},

        	render: function() {
        		num += 1;
        	}
        });

        model.set({
            item: 1
        });

        /**
         * Set data in model,
         * where data obj is predefined
         */

        expect(model.data[0].item).toBe(1);

        /**
         * render fucntion in Controller
         * invoces each time, when model is updated
         * (pub/sub pattern)
         */
        expect(num).toBe(2);

    });

});
